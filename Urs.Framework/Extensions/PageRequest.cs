﻿namespace Urs.Framework.Kendoui
{
    public class PageRequest
    {
        public int Page { get; set; }

        public int Limit { get; set; }

        public PageRequest()
        {
            this.Page = 1;
            this.Limit = 10;
        }
    }
}
