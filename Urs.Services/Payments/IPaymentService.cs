using System.Collections.Generic;
using Urs.Data.Domain.Orders;

namespace Urs.Services.Payments
{
    public partial interface IPaymentService
    {
        IList<IPaymentMethod> LoadActivePaymentMethods();

        IPaymentMethod LoadPaymentMethodBySystemName(string systemName);

        IList<IPaymentMethod> LoadAllPaymentMethods();



        ProcessPaymentResult ProcessPayment(ProcessPaymentRequest processPaymentRequest);

        void PostProcessPayment(PostProcessPaymentRequest postProcessPaymentRequest);

        bool CanRePostProcessPayment(Order order);


        decimal GetAdditionalHandlingFee(decimal subTotal, string paymentMethodSystemName);



        bool SupportCapture(string paymentMethodSystemName);

        CapturePaymentResult Capture(CapturePaymentRequest capturePaymentRequest);



        bool SupportPartiallyRefund(string paymentMethodSystemName);

        bool SupportRefund(string paymentMethodSystemName);

        RefundPaymentResult Refund(RefundPaymentRequest refundPaymentRequest);



        bool SupportVoid(string paymentMethodSystemName);

        VoidPaymentResult Void(VoidPaymentRequest voidPaymentRequest);

        PaymentMethodType GetPaymentMethodType(string paymentMethodSystemName);

        string GetMaskedCreditCardNumber(string creditCardNumber);
        
    }
}
