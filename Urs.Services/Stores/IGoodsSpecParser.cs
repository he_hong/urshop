using System.Collections.Generic;
using Urs.Data.Domain.Stores;

namespace Urs.Services.Stores
{
    public partial interface IGoodsSpecParser
    {
        #region Goods spec

        IList<int> ParseGoodsSpecIds(string attributes);

        IList<GoodsSpecMapping> ParseGoodsSpecs(string attributes);

        IList<GoodsSpecValue> ParseGoodsSpecValues(string attributes);

        IList<string> ParseValues(string attributes, int goodsSpecMappingId);

        string AddGoodsSpec(string attributes, GoodsSpecMapping pva, string value);

        bool AreGoodsSpecsEqual(string attributes1, string attributes2);

        GoodsSpecCombination FindGoodsSpecCombination(Goods goods,
            string attributesXml);

        #endregion

    }
}
