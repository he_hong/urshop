﻿using System.Collections.Generic;
using System.IO;
using Urs.Data.Domain.Stores;
using Urs.Data.Domain.Users;
using Urs.Data.Domain.Orders;
using Urs.Data.Domain.Agents;

namespace Urs.Services.ExportImport
{
    public partial interface IExportManager
    {

        void ExportGoodssToXlsx(Stream stream, IList<Goods> list);

        void ExportOrdersToXlsx(Stream stream, IList<Order> orders);
        
        void ExportUsersToXlsx(Stream stream, IList<User> users);

        string ExportBrandsToXml(IList<Brand> brands);

        byte[] ExportBrandsToXlsx(IEnumerable<Brand> brands);

        void ExportAgentBonusToXlsx(Stream stream, IList<AgentBonus> list, User user);

        void ExportAgentsToXlsx(Stream stream, IList<User> users);
    }
}
