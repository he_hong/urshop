﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Urs.Framework.Models;

namespace Urs.Admin.Models.Banners
{
    public partial class BannerItemAddPopupModel : BaseEntityModel
    {
        [DisplayName("标题")]
        public virtual string Title { get; set; }

        [DisplayName("副标题")]
        public virtual string SubTitle { get; set; }
        [DisplayName("链接")]
        public virtual string Url { get; set; }
        [DisplayName("图片")]
        [UIHint("Picture")]
        public int PictureId { get; set; }
        public int BannerSliderZoneId { get; set; }
        [DisplayName("排序")]
        public virtual int DisplayOrder { get; set; }
    }
}