﻿using System.Collections.Generic;

namespace Urs.Admin.Models.Stores
{
    public class PreparePic
    {
        public int pictureId { get; set; }
        public string imageUrl { get; set; }
    }

    public class PrepareSpec
    {
        public int id { get; set; }
        public string name { get; set; }
        public int valid { get; set; }
        public string valname { get; set; }
    }

    public class PrepareAttr
    {
        public PrepareAttr()
        {
            attrValArr = new List<GoodsAttrValue>();

        }
        public int value { get; set; }
        public string name { get; set; }
        public IList<GoodsAttrValue> attrValArr { get; set; }

        public partial class GoodsAttrValue
        {
            public int value { get; set; }
            public string name { get; set; }
        }
    }

    public class AttrTable
    {
        public AttrTable()
        {
            arrs = new List<AttrValueItem>();
            info = new AttrInfo();
        }
        public AttrInfo info { get; set; }

        public IList<AttrValueItem> arrs { get; set; }

        public partial class AttrInfo
        {
            public decimal price { get; set; }
            public int qty { get; set; }
            public string sku { get; set; }
        }

        public partial class AttrValueItem
        {
            public int value { get; set; }
            public string name { get; set; }
        }
    }

}
