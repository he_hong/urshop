﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Urs.Framework;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Stores
{
    public partial class GoodsReviewListModel : BaseModel
    {
        [UrsDisplayName("Admin.Store.GoodsReviews.List.CreateTimeFrom")]
        [UIHint("DateNullable")]
        public DateTime? CreateTimeFrom { get; set; }

        [UrsDisplayName("Admin.Store.GoodsReviews.List.CreateTimeTo")]
        [UIHint("DateNullable")]
        public DateTime? CreateTimeTo { get; set; }

        [UrsDisplayName("Admin.Store.GoodsReviews.List.SearchText")]
        
        public string SearchText { get; set; }

        [UrsDisplayName("Admin.Store.GoodsReviews.List.SearchGoods")]
        public int SearchGoodsId { get; set; }
        public int? GoodsId { get; set; }
        public bool? IsApproved { get; set; }
    }
}