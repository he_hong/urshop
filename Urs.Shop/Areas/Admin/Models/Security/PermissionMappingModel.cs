﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using Urs.Admin.Models.Users;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Security
{
    public partial class PermissionMappingModel : BaseModel
    {
        public PermissionMappingModel()
        {
            AvailablePermissions = new List<PermissionRecordModel>();
            AvailableUserRoles = new List<UserRoleModel>();
            Allowed = new Dictionary<string, IDictionary<int, bool>>();
            AvailableGroup = new List<SelectListItem>();
        }
        public IList<PermissionRecordModel> AvailablePermissions { get; set; }
        public IList<SelectListItem> AvailableGroup { get; set; }
        public IList<UserRoleModel> AvailableUserRoles { get; set; }

        //[permission system name] / [user role id] / [allowed]
        public IDictionary<string, IDictionary<int, bool>> Allowed { get; set; }
    }
}