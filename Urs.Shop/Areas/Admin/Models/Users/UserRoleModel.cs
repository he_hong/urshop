﻿using FluentValidation.Attributes;
using System.Collections.Generic;
using Urs.Admin.Validators.Users;
using Urs.Framework;
using Urs.Framework.Models;

namespace Urs.Admin.Models.Users
{
    [Validator(typeof(UserRoleValidator))]
    public partial class UserRoleModel : BaseEntityModel
    {
        public UserRoleModel()
        {
            PermissionNames = new List<string>();
            Permissions = new List<PermissionModel>();
        }
        [UrsDisplayName("Admin.Users.UserRoles.Fields.Name")]

        public string Name { get; set; }

        [UrsDisplayName("Admin.Users.UserRoles.Fields.Active")]
        public bool Active { get; set; }

        [UrsDisplayName("Admin.Users.UserRoles.Fields.IsSystemRole")]
        public bool IsSystemRole { get; set; }

        [UrsDisplayName("Admin.Users.UserRoles.Fields.SystemName")]
        public string SystemName { get; set; }

        [UrsDisplayName("Admin.Users.UserRoles.Fields.Permissions")]
        public IList<PermissionModel> Permissions { get; set; }

        public IList<string> PermissionNames { get; set; }

        public partial class PermissionModel
        {
            public string Name { get; set; }
            public string SystemName { get; set; }
            public bool Checked { get; set; }
        }
    }
}