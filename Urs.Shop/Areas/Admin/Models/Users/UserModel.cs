﻿using FluentValidation.Attributes;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Urs.Admin.Validators.Users;
using Urs.Framework;
using Urs.Framework.Models;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Users
{
    public partial class UserModel : BaseEntityModel
    {
        public UserModel()
        {
            SelectedUserRoleIds = new List<int>();
            AvailableUserRoles = new List<SelectListItem>();
            AssociatedExternalAuthRecords = new List<AssociatedExternalAuthModel>();
            AvailableUserLevels = new List<SelectListItem>();
        }

        [UrsDisplayName("Admin.Users.Users.Fields.Approved")]

        public bool Approved { get; set; }

        [UrsDisplayName("Admin.Users.Users.Fields.InvitedCode")]
        public string InvitedCode { get; set; }
        [UrsDisplayName("Admin.Users.Users.Fields.Code")]

        public string Code { get; set; }
        [UrsDisplayName("Admin.Users.Users.Fields.AvatarPictureUrl")]
        public string AvatarPictureUrl { get; set; }

        [UrsDisplayName("Admin.Users.Users.Fields.Email")]

        public string Email { get; set; }
        [UrsDisplayName("Admin.Users.Users.Fields.PhoneNumber")]

        public string PhoneNumber { get; set; }
        [UrsDisplayName("Admin.Users.Users.Fields.UserGuid")]
        public Guid UserGuid { get; set; }
        public IList<SelectListItem> AvailableUserLevels { get; set; }
        [UrsDisplayName("Admin.Users.Users.Fields.Password")]
        public string Password { get; set; }
        [UIHint("Picture")]
        [UrsDisplayName("Admin.Users.Users.Fields.AvatarPictureId")]
        public int AvatarPictureId { get; set; }
        //form fields & properties
        public bool GenderEnabled { get; set; }
        [UrsDisplayName("Admin.Users.Users.Fields.Gender")]
        public string Gender { get; set; }
        [UrsDisplayName("Admin.Users.Users.Fields.Nickname")]
        public string Nickname { get; set; }
        [UrsDisplayName("Admin.Users.Users.Fields.FullName")]
        public string FullName { get; set; }
        [UrsDisplayName("Admin.Users.Users.Fields.Phone")]

        public string Phone { get; set; }

        [UrsDisplayName("Admin.Users.Users.Fields.AdminComment")]
        public string AdminComment { get; set; }

        [UrsDisplayName("Admin.Users.Users.Fields.Active")]
        public bool Active { get; set; }

        [UrsDisplayName("Admin.Users.Users.Fields.Affiliate")]
        public int? AffiliateId { get; set; }
        public string AffilateName { get; set; }

        public int ChildNumber { get; set; }
        //registration date
        [UrsDisplayName("Admin.Users.Users.Fields.CreateTime")]
        public DateTime CreateTime { get; set; }
        [UrsDisplayName("Admin.Users.Users.Fields.LastActivityDate")]
        public DateTime LastActivityDate { get; set; }

        //IP adderss
        [UrsDisplayName("Admin.Users.Users.Fields.IPAddress")]
        public string LastIpAddress { get; set; }
        
        //user roles
        [UrsDisplayName("Admin.Users.Users.Fields.UserRoles")]
        public string SystemRoleNames { get; set; }
        public List<SelectListItem> AvailableUserRoles { get; set; }
        [UrsDisplayName("Admin.Users.Users.Fields.SelectedUserRoleIds")]
        public List<int> SelectedUserRoleIds { get; set; }
        public bool AllowManagingUserRoles { get; set; }

        //reward points history
        public bool DisplayRewardPointsHistory { get; set; }

        [UrsDisplayName("Admin.Users.Users.RewardPoints.Fields.AddRewardPointsValue")]
        public int AddRewardPointsValue { get; set; }

        [UrsDisplayName("Admin.Users.Users.RewardPoints.Fields.AddRewardPointsMessage")]

        public string AddRewardPointsMessage { get; set; }

        [UrsDisplayName("Admin.Users.Users.AssociatedExternalAuth")]
        public IList<AssociatedExternalAuthModel> AssociatedExternalAuthRecords { get; set; }
        [UrsDisplayName("Admin.Users.Users.HasWeChatAuth")]
        public bool HasWeChatAuth { get; set; }

        public int ShopId { get; set; }

        #region Nested classes

        public partial class AssociatedExternalAuthModel : BaseEntityModel
        {
            [UrsDisplayName("Admin.Users.AssociatedExternalAuth.Fields.Name")]
            public string Name { get; set; }

            [UrsDisplayName("Admin.Users.AssociatedExternalAuth.Fields.ExternalIdentifier")]
            public string OpenId { get; set; }

            public string AvatarUrl { get; set; }

            [UrsDisplayName("Admin.Users.AssociatedExternalAuth.Fields.AuthMethodName")]
            public string AuthMethodName { get; set; }
        }

        public partial class RewardPointsHistoryModel : BaseEntityModel
        {
            [UrsDisplayName("Admin.Users.Users.RewardPoints.Fields.Points")]
            public int Points { get; set; }

            [UrsDisplayName("Admin.Users.Users.RewardPoints.Fields.PointsBalance")]
            public int PointsBalance { get; set; }

            [UrsDisplayName("Admin.Users.Users.RewardPoints.Fields.Message")]

            public string Message { get; set; }

            [UrsDisplayName("Admin.Users.Users.RewardPoints.Fields.Date")]
            public DateTime CreateTime { get; set; }
        }
        public partial class UserOrderModel : BaseEntityModel
        {
            [UrsDisplayName("Admin.Users.Users.Orders.ID")]
            public override int Id { get; set; }

            [UrsDisplayName("Admin.Users.Users.Orders.OrderStatus")]
            public string OrderStatus { get; set; }
            [UrsDisplayName("Admin.Users.Users.Orders.OrderStatus")]
            public int OrderStatusId { get; set; }

            [UrsDisplayName("Admin.Users.Users.Orders.PaymentStatus")]
            public string PaymentStatus { get; set; }

            [UrsDisplayName("Admin.Users.Users.Orders.ShippingStatus")]
            public string ShippingStatus { get; set; }

            [UrsDisplayName("Admin.Users.Users.Orders.OrderTotal")]
            public string OrderTotal { get; set; }

            [UrsDisplayName("Admin.Users.Users.Orders.CreateTime")]
            public DateTime CreateTime { get; set; }
        }
        public partial class ActivityLogModel : BaseEntityModel
        {
            [UrsDisplayName("Admin.Users.Users.ActivityLog.ActivityLogType")]
            public string ActivityLogTypeName { get; set; }
            [UrsDisplayName("Admin.Users.Users.ActivityLog.Comment")]
            public string Comment { get; set; }
            [UrsDisplayName("Admin.Users.Users.ActivityLog.CreateTime")]
            public DateTime CreateTime { get; set; }
        }
        #endregion
    }
}