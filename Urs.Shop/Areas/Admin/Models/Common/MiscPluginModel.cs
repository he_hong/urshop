﻿using Microsoft.AspNetCore.Routing;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Common
{
    public partial class MiscPluginModel : BaseModel
    {
        public string FriendlyName { get; set; }

        public string ConfigurationActionName { get; set; }
        public string ConfigurationControllerName { get; set; }
        public RouteValueDictionary ConfigurationRouteValues { get; set; }
    }
}