﻿using Microsoft.AspNetCore.Mvc;
using Urs.Framework.Security;

namespace Urs.Admin.Controllers
{
    [Area("Admin")]
    [HttpsRequirement(SslRequirement.Yes)]
    public abstract partial class BaseAdminController : Controller
    {
        protected IActionResult HttpUnauthorized()
        {
            return RedirectToAction("Unauthorized", "Security");
        }

    }
}
