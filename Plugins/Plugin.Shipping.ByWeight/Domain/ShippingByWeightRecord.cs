using Urs.Core;
using Urs.Data.Domain.Directory;
using System.Collections.Generic;

namespace Urs.Plugin.Shipping.ByWeight.Domain
{
    /// <summary>
    /// Represents a shipping by weight record
    /// </summary>
    public partial class ShippingByWeightRecord : BaseEntity
    {

        private ICollection<ShippingScopeByWeightRecord> _scopes;

        /// <summary>
        /// Gets or sets the shipping method identifier
        /// </summary>
        public virtual int ShippingMethodId { get; set; }

        /// <summary>
        /// Gets or sets the "from" value
        /// </summary>
        public virtual decimal First { get; set; }

        /// <summary>
        /// Gets or sets the first price
        /// </summary>
        public virtual decimal FirstPrice { get; set; }

        /// <summary>
        /// Gets or sets the "to" value
        /// </summary>
        public virtual decimal Plus { get; set; }

        /// <summary>
        /// Gets or sets the plus price
        /// </summary>
        public virtual decimal PlusPrice { get; set; }

        /// <summary>
        /// Gets or sets the codes
        /// </summary>
        public virtual ICollection<ShippingScopeByWeightRecord> Scopes
        {
            get { return _scopes ?? (_scopes = new List<ShippingScopeByWeightRecord>()); }
            protected set { _scopes = value; }
        }

    }
}