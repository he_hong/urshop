﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Urs.Plugin.Weixin.MiniSDK.Models;
using Urs.Services.Configuration;
using Urs.Services.Security;
using Urs.Framework.Controllers;
using Weixin.SDK;
using Weixin.UrSDK.App;

namespace Urs.Plugin.Weixin.MiniSDK.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/wxminisdk")]
    [ApiController]
    public class WeixinMiniController : BaseApiController
    {
        private readonly WeixinMiniSDKSettings _openSKDSettings;
        private readonly ISettingService _settingService;
        private readonly IPermissionService _permissionService;

        public WeixinMiniController(IPermissionService permissionService,
            WeixinMiniSDKSettings openSDKSettings,
            ISettingService settingService)
        {
            this._openSKDSettings = openSDKSettings;
            this._settingService = settingService;
            this._permissionService = permissionService;
        }

        #region Methods
        [HttpPost("createQRCode")]
        public async Task<WeixinResult> createQRCode(WxacodeRequest request)
        {
            if (request == null)
                return new WeixinResult()
                {
                    IsSuccess = false,
                    Msg = "无参数",
                };

            var data = Newtonsoft.Json.JsonConvert.SerializeObject(request);

            var accessToken = BasicAPI.GetAccessToken(_openSKDSettings.AppId, _openSKDSettings.AppSecret);

            var result = new AppOAuth2API().CreateWXaQRCode(data, access_token: accessToken);

            return await result;
        }
        [HttpPost("get")]
        public async Task<WeixinResult> get(WxacodeRequest request)
        {
            if (request == null)
                return new WeixinResult()
                {
                    IsSuccess = false,
                    Msg = "无参数",
                };

            var data = Newtonsoft.Json.JsonConvert.SerializeObject(request);

            var accessToken = BasicAPI.GetAccessToken(_openSKDSettings.AppId, _openSKDSettings.AppSecret);

            var result = new AppOAuth2API().GetWXaCode(data, access_token: accessToken);

            return await result;
        }

        [HttpPost("getUnlimited")]
        public async Task<WeixinResult> getUnlimited(WxacodeRequest request)
        {
            if (request == null)
                return new WeixinResult()
                {
                    IsSuccess = false,
                    Msg = "无参数",
                };

            var data = Newtonsoft.Json.JsonConvert.SerializeObject(request);

            var accessToken = BasicAPI.GetAccessToken(_openSKDSettings.AppId, _openSKDSettings.AppSecret);

            var result = new AppOAuth2API().GetWXaCodeUnLimit(data, access_token: accessToken.access_token);

            return await result;
        }
        #endregion
    }
}