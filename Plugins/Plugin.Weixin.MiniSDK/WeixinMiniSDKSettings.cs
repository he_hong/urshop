﻿using Urs.Core.Configuration;

namespace Urs.Plugin.Weixin.MiniSDK
{
    public class WeixinMiniSDKSettings : ISettings
    {
        public string AppId { get; set; }
        public string AppSecret { get; set; }
    }
}