﻿using Urs.Core.Configuration;

namespace Plugin.Api
{
    public class OpenApiSettings : ISettings
    {
        public string Issuer => "Urselect";
        public string Audience => "www.urselect.com";
        public string JwtSecurityKey => "this is my custom Secret key for authnetication";
        public int Expires => 30;
    }
}
