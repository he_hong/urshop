﻿using Plugin.Api.Models.Common;
using System;
using System.Collections.Generic;

namespace Plugin.Api.Models.Agents
{
    /// <summary>
    /// 用户订单
    /// </summary>
    public partial class MoAgentOrder
    {
        public MoAgentOrder()
        {
            Items = new List<MoOrderItem>();
            Paging = new MoPaging();
        }
        /// <summary>
        /// 订单项
        /// </summary>
        public IList<MoOrderItem> Items { get; set; }
        /// <summary>
        /// 分页
        /// </summary>
        public MoPaging Paging { get; set; }

        #region Nested classes
        /// <summary>
        /// 订单项
        /// </summary>
        public partial class MoOrderItem
        {
            /// <summary>
            /// 订单Id
            /// </summary>
            public int OrderId { get; set; }
            /// <summary>
            /// 订单编号
            /// </summary>
            public string OrderCode { get; set; }
            /// <summary>
            /// 用户Id
            /// </summary>
            public int CustomerId { get; set; }
            /// <summary>
            /// 头像
            /// </summary>
            public string AvatarUrl { get; set; }
            /// <summary>
            /// 昵称
            /// </summary>
            public string Nickname { get; set; }
            /// <summary>
            /// 订单金额
            /// </summary>
            public string OrderTotal { get; set; }
            /// <summary>
            /// 订单状态
            /// </summary>
            public string OrderStatus { get; set; }
            /// <summary>
            /// 订单状态Id
            /// </summary>
            public int OrderStatusId { get; set; }
            /// <summary>
            /// 备注
            /// </summary>
            public string Remark { get; set; }
            /// <summary>
            /// 订单创建时间
            /// </summary>
            public DateTime CreatedTime { get; set; }
        }
        #endregion
    }
}