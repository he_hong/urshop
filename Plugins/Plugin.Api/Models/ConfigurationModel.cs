﻿using Urs.Framework;
using Urs.Framework.Mvc;

namespace Plugin.Api.Models
{
    public class ConfigurationModel : BaseModel
    {
        public string AppId { get; set; }
        public string AppSecret { get; set; }
        public string Domain { get; set; }
        public string IPWhiteList { get; set; }
        public bool EnabledHelpPage { get; set; }
    }
}