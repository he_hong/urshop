﻿namespace Plugin.Api.Models.Account
{
    /// <summary>
    /// 找回密码
    /// </summary>
    public class MoPasswordRecovery
    {
        /// <summary>
        /// 手机号
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// 验证码
        /// </summary>
        public string Token { get; set; }
        /// <summary>
        /// 新密码
        /// </summary>
        public string NewPassword { get; set; }
    }
}