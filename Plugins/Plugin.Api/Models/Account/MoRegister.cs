﻿using FluentValidation.Attributes;
using Plugin.Api.Validators.User;

namespace Plugin.Api.Models.Account
{
    /// <summary>
    /// 注册
    /// </summary>
    [Validator(typeof(MoRegisterValidator))]
    public partial class MoRegister
    {
        /// <summary>
        /// 手机/邮箱
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// 验证码
        /// </summary>
        public string Token { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// 邀请码
        /// </summary>
        public string InviteCode { get; set; }

    }
}