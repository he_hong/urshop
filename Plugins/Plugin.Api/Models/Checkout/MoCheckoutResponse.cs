﻿using System.Collections.Generic;
using Plugin.Api.Models.ShoppingCart;

namespace Plugin.Api.Models.Checkout
{
    /// <summary>
    /// 结算
    /// </summary>
    public class MoCheckoutResponse
    {
        public int OrderId { get; set; }
        public string PaymentMethod { get; set; }
    }
}