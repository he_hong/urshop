﻿namespace Plugin.Api.Models.Checkout
{
    /// <summary>
    /// 快捷订单数据
    /// </summary>
    public class MoQuickOrderResponse
    {
        /// <summary>
        /// 订单成功：订单Guid
        /// </summary>
        public string OrderGuid { get; set; }
        public int OrderId { get; set; }
    }
}