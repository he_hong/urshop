﻿namespace ExternalAuth.WeixinOpen.Models
{
    public class MoLogin
    {
        public string code { get; set; }
        public string state { get; set; }
        public string encryptedData { get; set; }
        public string iv { get; set; }
    }
}