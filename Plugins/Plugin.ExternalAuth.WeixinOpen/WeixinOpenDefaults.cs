﻿
namespace ExternalAuth.WeixinOpen
{
    /// <summary>
    /// Represents constants of the Weixin payment plugin
    /// </summary>
    public static class WeixinOpenDefaults
    {/// <summary>
     /// System name of the external authentication method
     /// </summary>
        public const string ProviderSystemName = "ExternalAuth.WeixinOpen";
    }
}