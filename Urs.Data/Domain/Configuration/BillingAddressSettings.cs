﻿using Urs.Core.Configuration;

namespace Urs.Data.Domain.Configuration
{
    public class BillingAddressSettings : ISettings
    {
        public bool BillingEnabled { get; set; }
    }
}
