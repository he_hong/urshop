﻿using Urs.Core.Configuration;

namespace Urs.Data.Domain.Configuration
{
    public class StoreSettings : ISettings
    {
        public bool ShowGoodsSku { get; set; }

        public bool GoodsSearchFullDescEnabled { get; set; }
       
        public int GoodsSearchTermMinimumLength { get; set; }

        public int SearchPageGoodssPerPage { get; set; }

        public bool IncludeFeaturedGoodssInNormalLists { get; set; }

        public FeaturedGoodsOrderBy FeaturedGoodsOrderBy { get; set; }
    }
    public enum FeaturedGoodsOrderBy : int
    {

        CreateTime = 0,
        Review = 10
    }
}