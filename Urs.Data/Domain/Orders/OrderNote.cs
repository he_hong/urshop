using Urs.Core;
using System;

namespace Urs.Data.Domain.Orders
{
    /// <summary>
    /// 订单记录
    /// </summary>
    public partial class OrderNote : BaseEntity
    {
        /// <summary>
        /// 订单Id
        /// </summary>
        public virtual int OrderId { get; set; }

        /// <summary>
        /// 记录
        /// </summary>
        public virtual string Note { get; set; }
        /// <summary>
        /// 显示给客户看
        /// </summary>
        public virtual bool DisplayToUser { get; set; }

        public virtual DateTime CreateTime { get; set; }

        public virtual Order Order { get; set; }
    }

}
