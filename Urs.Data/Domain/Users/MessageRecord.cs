﻿using System;
using Urs.Core;

namespace Urs.Data.Domain.Users
{
    public class MessageRecord : BaseEntity
    {
        /// <summary>
        /// 编号
        /// </summary>
        public virtual string Code { get; set; }
        /// <summary>
        /// 短信内容
        /// </summary>
        public virtual string Content { get; set; }
        /// <summary>
        /// 发送人Id
        /// </summary>
        public virtual int UserId { get; set; }
        /// <summary>
        /// 接收号码
        /// </summary>
        public virtual string Phone { get; set; }
        /// <summary>
        /// 发送时间
        /// </summary>
        public virtual DateTime SendTime { get; set; }
        /// <summary>
        /// 标志
        /// </summary>
        public virtual string Mark { get; set; }

    }
}
