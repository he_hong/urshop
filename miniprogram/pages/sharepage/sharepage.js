// pages/sharepage/sharepage.js
var WxParse = require('../../wxParse/wxParse.js')

import api from '../../api/api'
import {
  topic
} from '../../api/conf'

Page({

  data: {
    info: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.getnotip()
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },
  getnotip: function(){
    var that = this
    api.get(topic,{
      systemName: 'shareInfo'
    }).then(res=>{
      that.setData({
        info: res.Data
      })
      var notic = res.Data.Body;
      WxParse.wxParse('article', 'html', notic, that, 5);
    })
  }
})