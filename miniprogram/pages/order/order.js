import api from '../../api/api'
import {
  orderList
} from '../../api/conf'
import {
  ordercancel
} from '../../api/conf'
import {
  paymentweixinwap
} from '../../api/conf'
import {
  deliver
} from '../../api/conf'

Page({
  data: {
    orderList: [],
    orderStutas: 0,
    reTurn: false,
    page: 1
  },
  onLoad: function(options) {
    // 页面初始化 options为页面跳转所带来的参数
    wx.showLoading({
      title: '加载中',
      mask: true
    })
  },
  onReady: function() {
    // 页面渲染完成
    this.getOrderList()
  },
  getOrderList: function() {
    var that = this
    if (this.data.reTurn) {
      return
    }
    wx.showNavigationBarLoading()
    api.get(orderList, {
      orderstatus: that.data.orderStutas,
      page: that.data.page
    }).then(res => {
      that.setData({
        orderList: that.data.orderList.concat(res.Data.Items)
      })
      wx.hideLoading()
      wx.hideNavigationBarLoading()
      if (res.Data.Items.length < 12) {
        that.setData({
          reTurn: true
        })
        wx.showToast({
          title: '没有更多了',
          icon: 'none'
        })
      }
    }).catch(err => {
      wx.showToast({
        title: err.message,
        icon: 'none'
      })
    })
  },
  Confirm: function(e) {
    var that = this;
    wx.showModal({
      title: '提示',
      content: '确定收货',
      success: function(res) {
        if (res.confirm) {
          api.post(deliver, {
            Code: '' + e.currentTarget.dataset.code
          }).then(res => {

            wx.showToast({
              title: '收货成功'
            })
            this.getOrderList()
          }).catch(err => {

          })
        } else {

        }
      }
    });
  },
  openConfirm: function(e) {
    var that = this
    wx.showModal({
      title: '提示',
      content: '确定取消订单？',
      success: function(res) {
        if (res.confirm) {
          api.post(ordercancel, {
            CustomerGuid: wx.getStorageSync('guid'),
            OrderGuid: e.currentTarget.dataset.orderguid
          }).then(res => {
            wx.showToast({
              title: '订单取消成功'
            })
            that.setData({
              reTurn: false,
              page: 1,
              orderList: []
            })
            that.onReady()
          }).catch(err => {
            wx.hideNavigationBarLoading()
            wx.showToast({
              title: '订单取消失败',
              icon: 'none'
            })
          })
        } else {

        }
      }
    });
  },
  orderTap: function(e) {
    this.setData({
      orderStutas: e.currentTarget.dataset.stutas,
      orderList: [],
      page: 1,
      reTurn: false
    })
    this.getOrderList()
  },
  lower: function(e) {
    if (this.data.reTurn) {
      return
    }
    this.setData({
      page: this.data.page + 1
    })
    this.getOrderList()
  },
  pay: function(e) {
    wx.showLoading({
      title: '支付中',
    })
    api.get(paymentweixinwap, {
      orderId: e.currentTarget.dataset.orderid,
      paytype: 'Order'
    }).then(res => {
      wx.hideLoading()
      if (res.Code == 200) {
        let rep = res.Data.KeyValue
        wx.requestPayment({
          timeStamp: rep.timeStamp,
          nonceStr: rep.nonceStr,
          package: rep.package,
          signType: rep.signType,
          paySign: rep.paySign,
          success(res) {
            wx.showToast({
              title: '支付成功！',
              icon: 'success'
            })
            wx.switchTab({
              url: '/pages/my/my'
            })
          },
          fail(res) {
            wx.showToast({
              title: '支付失败！',
              icon: 'success'
            })
          }
        })
      }
    }).catch(err => {
      wx.showToast({
        title: '支付失败',
        icon: 'none'
      })
    })
  },
  saleClick: function(e) {
    let orderId = e.currentTarget.dataset.orderid
    wx.navigateTo({
      url: '/pages/salePage/salePage?orderid=' + orderId,
    })
  }
})